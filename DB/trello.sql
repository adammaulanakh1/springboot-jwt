-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.5.10-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping structure for table trello.board
CREATE TABLE IF NOT EXISTS `board` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `description` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table trello.board: ~5 rows (approximately)
/*!40000 ALTER TABLE `board` DISABLE KEYS */;
REPLACE INTO `board` (`id`, `name`, `description`) VALUES
	(1, 'boardAdam', 'boardAdamDetail'),
	(2, 'boardAdamMaulana', 'boardAdamDetail'),
	(3, 'boardAdamMaulanaKhosasih', 'boardAdamDetail'),
	(4, 'Brian', 'BoardBrian'),
	(5, 'boardAdamMaulana', 'boardAdamDetail');
/*!40000 ALTER TABLE `board` ENABLE KEYS */;

-- Dumping structure for table trello.card
CREATE TABLE IF NOT EXISTS `card` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nama_card` varchar(50) DEFAULT NULL,
  `list_posting_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_card_list` (`list_posting_id`),
  CONSTRAINT `FK_card_list` FOREIGN KEY (`list_posting_id`) REFERENCES `list` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table trello.card: ~2 rows (approximately)
/*!40000 ALTER TABLE `card` DISABLE KEYS */;
REPLACE INTO `card` (`id`, `nama_card`, `list_posting_id`) VALUES
	(1, 'card baru nih', 5),
	(3, 'Card baru', 5),
	(4, 'Card Brian', 5);
/*!40000 ALTER TABLE `card` ENABLE KEYS */;

-- Dumping structure for table trello.list
CREATE TABLE IF NOT EXISTS `list` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nama_list` varchar(255) DEFAULT NULL,
  `board_posting_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKrnoddcrhueckvwxlnutx9rs2c` (`board_posting_id`),
  CONSTRAINT `FKrnoddcrhueckvwxlnutx9rs2c` FOREIGN KEY (`board_posting_id`) REFERENCES `board` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- Dumping data for table trello.list: ~6 rows (approximately)
/*!40000 ALTER TABLE `list` DISABLE KEYS */;
REPLACE INTO `list` (`id`, `nama_list`, `board_posting_id`) VALUES
	(1, 'list saya', 1),
	(2, 'ini list board saya', 2),
	(3, 'ini list board saya', 3),
	(5, 'List Brian', 4),
	(8, 'list baru', 1),
	(9, 'list saya', 1);
/*!40000 ALTER TABLE `list` ENABLE KEYS */;

-- Dumping structure for table trello.roles
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table trello.roles: ~2 rows (approximately)
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
REPLACE INTO `roles` (`id`, `name`) VALUES
	(1, 'ROLE_USER'),
	(2, 'ROLE_MODERATOR'),
	(3, 'ROLE_ADMIN'),
	(4, 'ROLE_SUPER_ADMIN');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Dumping structure for table trello.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(50) DEFAULT NULL,
  `password` varchar(120) DEFAULT NULL,
  `username` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UKr43af9ap4edm43mmtq01oddj6` (`username`),
  UNIQUE KEY `UK6dotkott2kjsp8vw4d0m25fb7` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

-- Dumping data for table trello.users: ~17 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
REPLACE INTO `users` (`id`, `email`, `password`, `username`) VALUES
	(2, 'mod@bezkoder.com', '$2a$10$a6tgNuKvpYtQf0g30wWQvevqEzdBTBguMap8wrCvgI2zESPDjRIya', 'mod'),
	(3, 'adam@gmail.com', '$2a$10$4Ekjq2K0ftiIZ7o1JTC.9emuzNCRbTS7LCBmcqQ9Pwvr8FnTpN8Gy', 'adam'),
	(4, 'adam@root.com', '$2a$10$V17pOJhnPnxSm8ZuspVN3OGr0tFeduepdppZbBr4dYvKGvsmZ9HK.', 'adam-root.com'),
	(5, 'admin@root.com', '$2a$10$7.W.16e5TAw7BylFYfjuTOxWnWnztCfkg8ANVui7fD.9qjkwgLFgq', 'admin-root.com'),
	(6, 'maulana@root.com', '$2a$10$oSa16i1oOHTMb4NnVIL9B.c6Vq7U9qmyDRx.GlABtjU9EqxrakOVe', 'maulana-root.com'),
	(7, 'khosasih@root.com', '$2a$10$nyqUj2Dmz/gx6Bh0Zixw7eUCw9tzqVVOJAHgeXuiyKmapoaUhrN22', 'khosasih-root.com'),
	(8, 'superadmin@root.com', '$2a$10$vuubi0WgmCd1U9DRzRZXNuTsXBADMWpVsjDApetObaHyH3k32Vloi', 'superadmin-root.com'),
	(9, 'test@root.com', '$2a$10$q7ENE2ZzmUjb9Y/5FXw24.dCYxKfa5qnWp2D.HUmwAlUa11fadlh.', 'test-root.com'),
	(10, 'Brian@thrm.com', '$2a$10$LFvy.2n.KXGb/Tc/yseq0.kW7f4LY7Af2u6UD.BOy6fFEdK0wu6fS', 'Brian'),
	(11, 'Brianadmin@thrm.com', '$2a$10$m9q64pEJQcZJ0unyGgdHq.tbmRp1Go66NhUIgWo7AkDNJPdXHWoO2', 'BrianTHRM'),
	(12, 'khosasih@thrm.com', '$2a$10$5l5x4wufgO16JSS3kVaqU.Nik27p9rymAdaDI7zGgIq4exSJPH/0S', 'adammaulanakhosasih'),
	(13, 'test1@thrm.com', '$2a$10$5Kw5LtHlf3sAaQOJ7CE9e.moq1XAlCslF3ycIN3CDzhrQ1Y8a/1c.', 'test12'),
	(14, 'test12@thrm.com', '$2a$10$gxONmvX.By8qj8NXoOk6AeHkuYQ.GskGdS69T9aeDkanPKfIwAek6', 'test123'),
	(15, 'test123@thrm.com', '$2a$10$91dtM30ct6Y6POAVCVAJMOkxnhEohI9zxcRfanT/CEIXL821zTm.K', 'test1234'),
	(16, 'tes@thrm.com', '$2a$10$lea1cMv3C9tYUYy.C6SEX.77QVpNI8ILJYXMIgFWSWyEzohD7L9.O', 'tes'),
	(17, 'tes1@thrm.com', '$2a$10$18WVL9JeOPOTyEkSClxvVeeeWzCBEiSBKoA7DFVpxWpNNXFt02gc.', 'tes1'),
	(18, 'tes13@thrm.com', '$2a$10$sFE.PrGrqrBXRj6XP.Jpju1QuACxmfAMwNjjzwho8NUutc6OmSOrq', 'tes13'),
	(19, 'kkkkkkkkkkkkkkk@gmail.com', '$2a$10$XnTsjHaRoqnjZS2M2Hu5WeGU5vz.VpLzM2UX2Waz1MYr5QJ1g276W', 'kkkkkkkkkkkkkkk'),
	(20, 'tes135@thrm.com', '$2a$10$JwIgMtocbkfoT6Drp4ptZeZRbZ15y/3JpyY5Lm0O12m2EoFecnXRO', 'tes135'),
	(21, 'tes1357@thrm.com', '$2a$10$TfCX5TJRD2l7/VtA41YQ3OO4SdrZIc1rL1BGpHh.0hq.rASjuQO1G', 'tes1357'),
	(22, 'tes13579@thrm.com', '$2a$10$co/KDdIOog6xUDtAMUg5s.ci5u4w1yXmgPhtnTxw5UjclPH.ovftS', 'tes13579');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping structure for table trello.user_roles
CREATE TABLE IF NOT EXISTS `user_roles` (
  `user_id` bigint(20) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `FKh8ciramu9cc9q3qcqiv4ue8a6` (`role_id`),
  CONSTRAINT `FKh8ciramu9cc9q3qcqiv4ue8a6` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`),
  CONSTRAINT `FKhfh9dx7w3ubf1co1vdev94g3f` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table trello.user_roles: ~18 rows (approximately)
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;
REPLACE INTO `user_roles` (`user_id`, `role_id`) VALUES
	(2, 1),
	(2, 2),
	(3, 3),
	(4, 1),
	(5, 1),
	(6, 1),
	(7, 3),
	(8, 1),
	(9, 4),
	(10, 4),
	(11, 4),
	(12, 1),
	(13, 1),
	(14, 4),
	(15, 1),
	(16, 3),
	(17, 4),
	(18, 1),
	(19, 1),
	(20, 1),
	(21, 1),
	(22, 3);
/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
