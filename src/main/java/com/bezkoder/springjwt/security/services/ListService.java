package com.bezkoder.springjwt.security.services;


import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import com.bezkoder.springjwt.dto.RequestPostingList;
import com.bezkoder.springjwt.models.BoardPosting;
import com.bezkoder.springjwt.models.CardPosting;
import com.bezkoder.springjwt.models.ListPosting;
import com.bezkoder.springjwt.repository.BoardRepository;
import com.bezkoder.springjwt.repository.CardRepository;
import com.bezkoder.springjwt.repository.ListRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ListService {
    @Autowired
    ListRepository listRepo;
    @Autowired
    BoardRepository boardRepo;
    // @Autowired
    // CardRepository cardRepo;

    public List<ListPosting> listLits() {
        ListPosting list = new ListPosting();

        listRepo.save(list);

        List<ListPosting> postings = new ArrayList<>();
        postings = listRepo.findAll();
        return postings;

    }
    

    @Transactional
    public String saveList(RequestPostingList req) {
        ListPosting list = new ListPosting();
        list.setNama_list(req.getNama_list());
        // list.setBoard_posting_id(req.getBoard_posting_id());
        ////////////////////// One To
        BoardPosting board = boardRepo.findById(req.getId()).get();
        list.setBoardPosting(board);
        ////////////////////// Many
        // CardPosting card = cardRepo.findById(req.getId()).get();
        // list.getCardPostings(card);
        listRepo.save(list);

        return "List sukses";
    }


    public String deleteList(Long id){
        listRepo.deleteById(id);
        return "List Success Removed !!" + id;
    }
}
