package com.bezkoder.springjwt.security.services;

import com.bezkoder.springjwt.dto.RequestPosting;
import com.bezkoder.springjwt.models.BoardPosting;
import com.bezkoder.springjwt.models.Users;
import com.bezkoder.springjwt.repository.BoardRepository;
import com.bezkoder.springjwt.repository.UsersRepository;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class BoardService {
    @Autowired
    BoardRepository boardRepo;

    @Autowired
    UsersRepository usersRepo;

    public List<BoardPosting> listBoards() {
        BoardPosting board = new BoardPosting();

        boardRepo.save(board);

        List<BoardPosting> postings = new ArrayList<>();
        postings = boardRepo.findAll();
        return postings;

    }
    

    @Transactional
    public String saveBoard(RequestPosting req) {
        BoardPosting board = new BoardPosting();
        board.setName(req.getName());
        board.setDescription(req.getDescription());
        boardRepo.save(board);

        Users user = usersRepo.findById(req.getId()).get();
        board.setUser(user);

        return "Board sukses";
    }

    public String deleteBoard(Long id){
        boardRepo.deleteById(id);
        return "Board Success Removed !!" + id;
    }
}