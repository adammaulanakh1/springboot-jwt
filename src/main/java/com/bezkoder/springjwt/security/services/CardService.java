package com.bezkoder.springjwt.security.services;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import com.bezkoder.springjwt.dto.RequestPostingCard;
import com.bezkoder.springjwt.models.CardPosting;
import com.bezkoder.springjwt.models.ListPosting;
import com.bezkoder.springjwt.repository.CardRepository;
import com.bezkoder.springjwt.repository.ListRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CardService {
    @Autowired
    CardRepository cardRepo;
    @Autowired
    ListRepository listRepo;

    public List<CardPosting> cardCards() {
        CardPosting card =  new CardPosting();

        cardRepo.save(card);

        List<CardPosting> postings = new ArrayList<>();
        postings = cardRepo.findAll();
        return postings;
    }
    @Transactional
    public String saveCard(RequestPostingCard req){
        CardPosting card = new CardPosting();
            card.setNama_card(req.getNama_card());

            ListPosting list = listRepo.findById(req.getId()).get();
            card.setListPosting(list);
            cardRepo.save(card);

            cardRepo.save(card);

            return "Card sukses";
    }

    public String deleteCard(Long id){
        cardRepo.deleteById(id);
        return " Card Success Removed !!" + id;
    }
}
