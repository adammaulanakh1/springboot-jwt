package com.bezkoder.springjwt.controllers;

import java.util.List;
import java.util.Optional;

import com.bezkoder.springjwt.dto.RequestPostingList;
import com.bezkoder.springjwt.models.CardPosting;
import com.bezkoder.springjwt.models.ListPosting;
import com.bezkoder.springjwt.repository.CardRepository;
import com.bezkoder.springjwt.repository.ListRepository;
import com.bezkoder.springjwt.security.services.ListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/list")
public class ListController {
    @Autowired
    ListRepository listRepository;
    @Autowired
    CardRepository cardRepository;

    @Autowired
    ListService listService;

    @GetMapping("/listProject")
    public ResponseEntity<?> getList() {
        List<ListPosting> post = listRepository.findAll();
        return ResponseEntity.ok(post);
    }

    @PostMapping("/posting")
    public ResponseEntity<?> postingList(@RequestBody RequestPostingList req) {
        String hasil = listService.saveList(req);
        return ResponseEntity.ok(hasil);
    }

    @DeleteMapping("/delete/{id}")
    public String deleteList(@PathVariable Long id) {
        return listService.deleteList(id);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<ListPosting> updateList(@PathVariable() Long id, @RequestBody RequestPostingList req) {
        Optional<ListPosting> _listData = listRepository.findById(id);

        if (_listData.isPresent()) {
            ListPosting list = _listData.get();
            list.setNama_list(req.getNama_list());
            // list.setBoard_posting_id(req.getBoard_posting_id());
            // listRepository.save(list);
            return ResponseEntity.ok(list);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
