package com.bezkoder.springjwt.controllers;

import java.util.List;
import java.util.Optional;

import com.bezkoder.springjwt.dto.RequestPostingCard;
import com.bezkoder.springjwt.models.CardPosting;
import com.bezkoder.springjwt.repository.CardRepository;
import com.bezkoder.springjwt.security.services.CardService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/card")
public class CardController {
    @Autowired
    CardRepository cardRepository;

    @Autowired
    CardService cardService;

    @GetMapping("/cardProject")
    public ResponseEntity<?> getCard(){
        List<CardPosting> post = cardRepository.findAll();
        return ResponseEntity.ok(post);
   
    }

    @GetMapping("/cardProject/{id}")
    public ResponseEntity<CardPosting> getListPostingID(@PathVariable() Long id){
      Optional <CardPosting> card = cardRepository.findById(id);
      if (card.isPresent()){
        return new ResponseEntity<>(card.get(), HttpStatus.OK);
    }else{
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
      }
    }

    @PostMapping("/posting")
    public ResponseEntity<?> postingCard(@RequestBody RequestPostingCard req){
        String hasil = cardService.saveCard(req);

        return ResponseEntity.ok(hasil);
    }

    @DeleteMapping("/delete/{id}")
    public String deleteCard(@PathVariable Long id){
        return cardService.deleteCard(id);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<CardPosting> updateCard(@PathVariable() Long id, @RequestBody RequestPostingCard req){
        Optional<CardPosting> _cardData = cardRepository.findById(id);

        if (_cardData.isPresent()){
            CardPosting card = _cardData.get();
            card.setNama_card(req.getNama_card());
            return new ResponseEntity<>(cardRepository.save(card), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
