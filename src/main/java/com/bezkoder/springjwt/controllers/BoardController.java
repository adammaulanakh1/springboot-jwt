package com.bezkoder.springjwt.controllers;

import java.util.List;
import java.util.Optional;
import com.bezkoder.springjwt.dto.RequestPosting;
import com.bezkoder.springjwt.models.BoardPosting;
import com.bezkoder.springjwt.repository.BoardRepository;
import com.bezkoder.springjwt.repository.UsersRepository;
import com.bezkoder.springjwt.security.services.BoardService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/board")
public class BoardController {
    @Autowired
    BoardRepository boardRepository;

    @Autowired
    BoardService boardService;

    @Autowired
    UsersRepository usersRepository;

    @GetMapping("/project")
    public ResponseEntity<?> getBoard(){
        List<BoardPosting> posts = boardRepository.findAll();
        // String userLogin =  SecurityUtil.getCurrentUserLogin().get();
        // Long userId = this.usersRepository.findOneByLogin(userLogin).get().getId();
        return ResponseEntity.ok(posts);
    }

    @GetMapping("/project/{id}")
    public ResponseEntity<BoardPosting> getBoardPostingID(@PathVariable() Long id){
      Optional <BoardPosting> board = boardRepository.findById(id);
      if (board.isPresent()){
        return new ResponseEntity<>(board.get(), HttpStatus.OK);
    }else{
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
      }
    }

    @PostMapping("/posting")
    public ResponseEntity<?> postingBoard(@RequestBody RequestPosting req){
        String hasil = boardService.saveBoard(req);

        return ResponseEntity.ok(hasil);
    }

    @DeleteMapping("/delete/{id}")
    public String deleteBoard(@PathVariable Long id){
        return boardService.deleteBoard(id);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<BoardPosting> updateBoard(@PathVariable() Long id, @RequestBody RequestPosting req) {
    Optional<BoardPosting> _postingData = boardRepository.findById(id);

    if (_postingData.isPresent()) {
        BoardPosting board = _postingData.get();
        board.setName(req.getName());
        board.setDescription(req.getDescription());
      return new ResponseEntity<>(boardRepository.save(board), HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }
}