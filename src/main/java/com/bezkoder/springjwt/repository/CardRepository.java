package com.bezkoder.springjwt.repository;

import java.util.List;

import com.bezkoder.springjwt.models.CardPosting;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface CardRepository extends JpaRepository<CardPosting, Long>{
    @Query("select card from CardPosting card where card.id < 10 ")
    List<CardPosting> findList();
    
}
