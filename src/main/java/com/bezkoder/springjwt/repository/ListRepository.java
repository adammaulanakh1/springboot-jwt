package com.bezkoder.springjwt.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.bezkoder.springjwt.models.ListPosting;

public interface ListRepository extends JpaRepository<ListPosting, Long>{
    @Query("select list from ListPosting list where list.id < 10")
    List<ListPosting> findList();
}
