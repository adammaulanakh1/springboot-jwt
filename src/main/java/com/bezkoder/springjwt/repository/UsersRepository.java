package com.bezkoder.springjwt.repository;

import java.util.List;

import com.bezkoder.springjwt.models.Users;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface UsersRepository extends JpaRepository<Users, Long> {
    @Query("select users from Users users where users.id < 10")
    List<Users> findList();
    
}
