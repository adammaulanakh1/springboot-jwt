package com.bezkoder.springjwt.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.bezkoder.springjwt.models.BoardPosting;

public interface BoardRepository extends JpaRepository<BoardPosting, Long>{
    @Query("select board from BoardPosting board where board.id < 10")
    List<BoardPosting> findList();
}