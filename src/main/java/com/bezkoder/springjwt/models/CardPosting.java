package com.bezkoder.springjwt.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

@Data
@Entity
@Table ( name = "card")
public class CardPosting {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    String nama_card;

    @ManyToOne
    @JoinColumn(name = "list_posting_id", referencedColumnName = "id" )
	@JsonIgnore
    private ListPosting listPosting;
}
