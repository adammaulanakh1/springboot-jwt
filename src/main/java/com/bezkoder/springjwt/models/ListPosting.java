package com.bezkoder.springjwt.models;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
@Entity
@Table( name = "list")
public class ListPosting  {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    String nama_list;
    
    @ManyToOne
    @JoinColumn(name = "board_posting_id", referencedColumnName = "id" )
	@JsonIgnore
    private BoardPosting boardPosting;

    @OneToMany(mappedBy = "listPosting", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<CardPosting> cardPostings;
}