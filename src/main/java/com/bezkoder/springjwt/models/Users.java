package com.bezkoder.springjwt.models;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
@Entity
@Table ( name = "users")
public class Users {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id; 
    String username;
    
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL,
    fetch = FetchType.LAZY)
    @JsonIgnore
    private Set<BoardPosting> boardPostings;

}
