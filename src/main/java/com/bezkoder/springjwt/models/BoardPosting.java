package com.bezkoder.springjwt.models;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
@Entity
@Table( name = "board")
public class BoardPosting {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    String name;
    String description;

    @OneToMany(mappedBy = "boardPosting",cascade = CascadeType.ALL, 
    fetch = FetchType.LAZY)
    @JsonIgnore
    private Set<ListPosting> listPostings;

    @ManyToOne
    @JoinColumn(name = "user_board_id", referencedColumnName = "id")
    // @JsonIgnore
    private Users user;
    
    
}