package com.bezkoder.springjwt.dto;

import lombok.Data;

@Data
public class RequestPostingCard {
    Long id;
    String nama_card;
}
