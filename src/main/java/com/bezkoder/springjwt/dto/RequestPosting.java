package com.bezkoder.springjwt.dto;

import lombok.Data;

@Data
public class RequestPosting {
    Long id;
    String name;
    String description;
}