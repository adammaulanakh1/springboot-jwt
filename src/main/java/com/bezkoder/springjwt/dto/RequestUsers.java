package com.bezkoder.springjwt.dto;

import lombok.Data;

@Data
public class RequestUsers {
    Long id;
    String username;
}
